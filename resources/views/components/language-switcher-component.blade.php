<select class="form-select" id="floatingSelect" aria-label="Default select example" onchange="location = this.value;">
    @foreach($locales as $locale)
        <option value="{{route('switcher', ['locale' => $locale])}}"
        {{$selected($locale)}}
        >
            {{__("interface." . $locale)}}
        </option>
    @endforeach
</select>
