<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PhraseTranslation extends Model
{
    public $timestamps = false;
    protected $fillable = ['content'];

    public function phrase()
    {
        $this->belongsTo(Phrase::class);
    }
}
