@extends('layouts.app')
@section('content')
    <div class="container">
        <div style="text-align: center">
            <h1>
                {{__('interface.phrase')}} {{$phrase->translate('ru')->content}}
            </h1>
        </div>
        <div>
            <form method="post" action="{{action([\App\Http\Controllers\PhraseController::class, 'store'], ['phrase' => $phrase])}}">
                @csrf
                <div>
                    <h2>
                        {{__('interface.locEnglish')}}
                    </h2>
                    @if(!isset($phrase->translate('en')->content))
                        @if(!$user == null)
                            <textarea type="text" name="phrase.en" id="phrase.en">{{old('phrase_en')}}</textarea><br>
                            @error('phrase_en')
                            <div>
                                <p class="text-danger">
                                    {{__('interface.error')}}
                                </p>
                            </div>
                            @enderror
                        @endif
                    @else
                        <div>
                            <p>{{$phrase->translate('en')->content}}</p>
                        </div>
                    @endif
                </div>
                <div>
                    <h2>
                        {{__('interface.locFrench')}}
                    </h2>
                    @if(!isset($phrase->translate('fr')->content))
                        @if(!$user == null)
                        <textarea type="text" name="phrase.fr" id="phrase.fr">{{old('phrase_fr')}}</textarea><br>
                            @error('phrase_fr')
                            <div>
                                <p class="text-danger">
                                    {{__('interface.error')}}
                                </p>
                            </div>
                            @enderror
                        @endif
                    @else
                        <div>
                            <p>{{$phrase->translate('fr')->content}}</p>
                        </div>
                    @endif
                </div>
                <div>
                    <h2>
                        {{__('interface.locGerman')}}
                    </h2>
                    @if(!isset($phrase->translate('de')->content))
                        @if(!$user == null)
                        <textarea type="text" name="phrase.de" id="phrase.de">{{old('phrase_de')}}</textarea><br>
                            @error('phrase_de')
                            <div>
                                <p class="text-danger">
                                    {{__('interface.error')}}
                                </p>
                            </div>
                            @enderror
                        @endif
                    @else
                        <div>
                            <p>{{$phrase->translate('de')->content}}</p>
                        </div>
                    @endif
                </div>
                <div>
                    <h2>
                        {{__('interface.locGreek')}}
                    </h2>
                    @if(!isset($phrase->translate('gr')->content))
                        @if(!$user == null)
                        <textarea type="text" name="phrase.gr" id="phrase.gr">{{old('phrase_gr')}}</textarea><br>
                            @error('phrase_gr')
                            <div>
                                <p class="text-danger">
                                    {{__('interface.error')}}
                                </p>
                            </div>
                            @enderror
                        @endif
                    @else
                        <div>
                            <p>{{$phrase->translate('gr')->content}}</p>
                        </div>
                    @endif
                </div>
                <div>
                    <h2>
                        {{__('interface.locItalian')}}
                    </h2>
                    @if(!isset($phrase->translate('it')->content))
                        @if(!$user == null)
                        <textarea type="text" name="phrase.it" id="phrase.it">{{old('phrase_it')}}</textarea><br>
                            @error('phrase_it')
                            <div>
                                <p class="text-danger">
                                    {{__('interface.error')}}
                                </p>
                            </div>
                            @enderror
                        @endif
                    @else
                        <div>
                            <p>{{$phrase->translate('it')->content}}</p>
                        </div>
                    @endif
                </div>
                @if(!$user == null)
                <button class="btn btn-warning" type="submit">{{__('interface.saveTranslations')}}</button>
                @endif
            </form>
    </div>
@endsection
