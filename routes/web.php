<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::middleware('language_switcher')->group(function () {

    Route::get('/', [\App\Http\Controllers\PhraseController::class, 'index'])->name('index');
    Route::resource('phrases', \App\Http\Controllers\PhraseController::class)->except('create', 'edit', 'store');
    Route::post('phrases/store/{phrase}', [\App\Http\Controllers\PhraseController::class, 'store']);
    Route::post('/phrases/make', [\App\Http\Controllers\PhraseController::class, 'make']);
    Auth::routes();

    Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
    Route::get('lang/{locale}', [\App\Http\Controllers\LanguageSwitchController::class, 'switcher'])->name('switcher')
        ->where('locale', 'en|ru');

});
