<?php

namespace Database\Seeders;

use App\Models\Phrase;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
Use Faker\Generator;
use Illuminate\Container\Container;

class PhraseTableSeeder extends Seeder
{
    protected $faker;

    public function __construct()
    {
        $this->faker = $this->withFaker();
    }
    protected function withFaker()
    {
        return Container::getInstance()->make(Generator::class);
    }

    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        \App\Models\Phrase::factory()->count(1)->create();
        Phrase::factory()->count(1)->create(['fr' => ['content' => $this->faker->sentence(10)], 'de' => ['content' => $this->faker->sentence(10)]]);
    }
}
