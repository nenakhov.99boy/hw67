@extends('layouts.app')
@section('content')
    <div class="container">
        <h1 style="text-align: center; margin-bottom: 50px">{{__('interface.title')}}</h1>
        <h2>{{__('interface.list')}}</h2>
        <div class="row">
            @foreach($phrases as $phrase)
                <div class="col-12" style="border: 1px solid black; font-size: 40px; border-radius: 10px; margin-bottom: 100px">
                    <p><a style="text-decoration: none; color: black" href="{{route('phrases.show', compact('phrase'))}}">{{$phrase->translate('ru')->content}}</a></p>
                </div>
            @endforeach
        </div>
        @if(!$user == null)
        <div style="text-align: center; margin-top: 200px">
            <h1>{{__('interface.send')}}</h1>
            <form method="post" action="{{action([\App\Http\Controllers\PhraseController::class, 'make'])}}">
                @csrf
                <textarea style="width: 500px; height: 100px" type="text" name="phrase.ru" id="phrase.ru"></textarea><br>
                @error('phrase_ru')
                <div>
                    <p class="text-danger">
                        {{__('interface.error')}}
                    </p>
                </div>
                @enderror
                <button class="btn btn-warning" type="submit">{{__('interface.sendButton')}}</button>
            </form>
        </div>
        @endif
        <div class="justify-content-md-center p-5">

            <div class="col-md-auto">

                {{ $phrases->links() }}

            </div>

        </div>
    </div>
@endsection
