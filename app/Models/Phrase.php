<?php

namespace App\Models;

use Astrotomic\Translatable\Translatable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Astrotomic\Translatable\Contracts\Translatable as TranslatableContract;


class Phrase extends Model implements TranslatableContract
{
    use HasFactory;
    use Translatable;

    protected $fillable = [
        'content'
    ];

    public $translatedAttributes = [
        'content'
    ];

    public function phraseTranslations()
    {
        return $this->hasMany(PhraseTranslation::class);
    }
}
