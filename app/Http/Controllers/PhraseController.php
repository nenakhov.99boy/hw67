<?php

namespace App\Http\Controllers;

use App\Models\Phrase;
use Astrotomic\Translatable\Validation\RuleFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PhraseController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $user = Auth::user();
        $phrases = Phrase::paginate(10);
        return view('index', compact('phrases', 'user'));
    }

    public function make(Request $request)
    {
        $rules = RuleFactory::make([
            'phrase_ru' => ['min:10', 'required', 'string', 'max:1000']
        ]);
        $validatedData = $request->validate($rules);
        $phrase = new Phrase();
        $phrase->fill([
            'ru' => [
                'content' => $validatedData['phrase_ru']
            ]
        ]);
        $phrase->save();
        return back();
    }
    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request, Phrase $phrase)
    {
        $rules = RuleFactory::make([
            'phrase_ru' => ['min:10', 'required', 'string', 'max:1000']
        ]);
        if (!$request['phrase_en'] == null)
        {
            $rules = RuleFactory::make([
                'phrase_en' => ['min:10', 'string', 'max:1500']
            ]);
            $validatedData = $request->validate($rules);
            $phrase->update([
                'en' => [
                    'content' => $validatedData['phrase_en']
            ]]);

        }
        if (!$request['phrase_fr'] == null)
        {
            $rules = RuleFactory::make([
                'phrase_fr' => ['min:10', 'string', 'max:1500']
            ]);
            $validatedData = $request->validate($rules);
            $phrase->update([
                'fr' => [
                    'content' => $validatedData['phrase_fr']
                ]]);
        }
        if (!$request['phrase_de'] == null)
        {
            $rules = RuleFactory::make([
                'phrase_de' => ['min:10', 'string', 'max:1500']
            ]);
            $validatedData = $request->validate($rules);
            $phrase->update([
                'de' => [
                    'content' => $validatedData['phrase_de']
                ]]);

        }
        if (!$request['phrase_gr'] == null)
        {
            $rules = RuleFactory::make([
                'phrase_gr' => ['min:10', 'string', 'max:1500']
            ]);
            $validatedData = $request->validate($rules);
            $phrase->update([
                'gr' => [
                    'content' => $validatedData['phrase_gr']
                ]]);
        }

        if (!$request['phrase_it'] == null)
        {
            $rules = RuleFactory::make([
                'phrase_it' => ['min:10', 'string', 'max:1500']
            ]);
            $validatedData = $request->validate($rules);
            $phrase->update([
                'it' => [
                    'content' => $validatedData['phrase_it']
                ]]);
        }
        $phrase->save()
;        return back();
    }

    /**
     * Display the specified resource.
     */
    public function show(Phrase $phrase)
    {
        $user = Auth::user();
        return view('phrases.show', compact('phrase', 'user'));
    }

}
